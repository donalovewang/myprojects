package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hi,Leo, I still like you, Why i can not forget you");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.show();
    }


    public static void main(String[] args) {
        try{
        String music= Main.class.getResource("WeDon'tTalkAnymore.mp3").toURI().toString();
        Media media=new Media(music);
        MediaPlayer player=new MediaPlayer(media);
        player.play();
        launch(args);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
